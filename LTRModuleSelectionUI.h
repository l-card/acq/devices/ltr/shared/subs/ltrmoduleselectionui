#ifndef LTRMODULESELECTION_H
#define LTRMODULESELECTION_H

#include <QObject>
#include <QStringList>
#include <QSharedPointer>

#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/devs/DeviceValidator.h"



class QLineEdit;
class QComboBox;
class QProgressDialog;

class LTRModuleSelectionUI : public QObject {
    Q_OBJECT
public:
    static QString defaultGroup() {return QStringLiteral("lastModule");}

    explicit LTRModuleSelectionUI(const QStringList &devs, QComboBox *crate,
                                  QComboBox *slot, QWidget *parent = nullptr,
                                  const QString &set_group = defaultGroup());

    explicit LTRModuleSelectionUI(const QSharedPointer<LQMeas::DeviceValidator> &validator,
                                  QComboBox *crate, QComboBox *slot, QWidget *parent = nullptr,
                                  const QString &set_group = defaultGroup());
    ~LTRModuleSelectionUI() override;


    const QSharedPointer<LQMeas::LTRModule> &currentModule() const {return m_dev;}
    const QList<QSharedPointer <LQMeas::LTRModule> > &moduleList() const {return m_devList;}

    void setDevOpenFlags(LQMeas::Device::OpenFlags flags);
    void setResetBtnName(const QString &name);
    void setNoModuleWarnEn(bool en) {m_no_module_warn_en = en;}
Q_SIGNALS:
    void beforeModuleChange();
    void moduleChanged(const QSharedPointer<LQMeas::LTRModule> &dev);
public Q_SLOTS:
    void reset(bool refresh = true);
    void refreshDevList(const QString &prefCrate = QString{}, int prefSlot = 0);
    void closeCurModule();

    void saveSettings();
private Q_SLOTS:
    void onCrateChanged();
    void updateCrate(int prefSlot = 0, bool try_reopen = false);
    void onModuleChanged(bool try_reopen = false);
    void onModuleOpProgr(LQMeas::Device::OpStatus status,  const QString &descr,
                         int done, int size, const LQError &err);

private:
    const QStringList &crateSerialList() const {return m_crateSerialList;}

    QComboBox *m_crateBox;
    QComboBox *m_slotBox;
    QStringList m_sup_devs;
    QString m_set_group;
    QList<QSharedPointer <LQMeas::LTRModule> > m_devList;
    QSharedPointer<LQMeas::LTRModule> m_dev;
    QStringList m_crateTypes;
    QStringList m_crateSerialList;
    QWidget *m_parent;
    QProgressDialog* m_progrDialog {nullptr};
    int m_dev_reopen_cnt {0};

    LQMeas::Device::OpenFlags m_open_flags {LQMeas::Device::OpenFlag::None};
    QString m_resetBtnName;
    bool m_no_module_warn_en {true};

    QSharedPointer<LQMeas::DeviceValidator> m_validator;
};

#endif // LTRMODULESELECTION_H
