#include "LTRModuleSelectionUI.h"
#include <QLineEdit>
#include <QComboBox>
#include <QSettings>
#include <QMessageBox>
#include <QProgressDialog>

#include "lqmeas/devs/DeviceInfo.h"
#include "lqmeas/devs/DeviceRef.h"
#include "lqmeas/devs/LTR/LTRResolver.h"
#include "lqmeas/devs/DeviceNameValidator.h"
#include "LQError.h"

static const QString ui_key_crate_serial { QStringLiteral("crateSerial")};
static const QString ui_key_crate_slot   { QStringLiteral("slot")};


LTRModuleSelectionUI::LTRModuleSelectionUI(const QStringList &devs, QComboBox *crate, QComboBox *slot,
                                           QWidget *parent, const QString &set_group) :
    LTRModuleSelectionUI{QSharedPointer<LQMeas::DeviceValidator>{new LQMeas::DeviceNameValidator{devs}},
                         crate, slot, parent, set_group} {
}

LTRModuleSelectionUI::LTRModuleSelectionUI(const QSharedPointer<LQMeas::DeviceValidator> &validator,
                                           QComboBox *crate, QComboBox *slot,
                                           QWidget *parent, const QString &set_group) :
    QObject{parent},
    m_crateBox{crate},
    m_slotBox{slot},
    m_set_group{set_group},
    m_parent{parent},
    m_resetBtnName{tr("Сброс")},
    m_validator{validator} {

}

LTRModuleSelectionUI::~LTRModuleSelectionUI() {
    closeCurModule();
    delete m_progrDialog;
}

void LTRModuleSelectionUI::setDevOpenFlags(LQMeas::Device::OpenFlags flags) {
    m_open_flags = flags;
}

void LTRModuleSelectionUI::setResetBtnName(const QString &name) {
    m_resetBtnName = name;
}

void LTRModuleSelectionUI::reset(bool refresh) {
    m_dev_reopen_cnt = 0;
    QString serial;
    int slot=0;
    if (m_dev) {
        slot = m_dev->slot();
        serial =  m_dev->ltrCrateRef().devInfo().serial();
    }
    closeCurModule();
    if (refresh)
        refreshDevList(serial, slot);
}

void LTRModuleSelectionUI::refreshDevList(const QString &prefCrate, int prefSlot) {
    disconnect(m_crateBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &LTRModuleSelectionUI::onCrateChanged);

    QString selCrate {prefCrate};

    if (selCrate.isEmpty() && !prefSlot) {
        QSettings set;
        set.beginGroup(m_set_group);
        selCrate = set.value(ui_key_crate_serial).toString();
        prefSlot = set.value(ui_key_crate_slot, 0).toInt();
        set.endGroup();
    }

    LQError err;
    m_devList = LQMeas::LTRResolver::resolver().getModuleList(*m_validator, err);
    m_crateSerialList.clear();
    m_crateTypes.clear();
    int fnd_ind = 0;

     for (const QSharedPointer<LQMeas::LTRModule> &dev : moduleList()) {
         LQError devError;
         dev->open(m_open_flags, devError);
         if (dev->isOpened()) {
             const QString crateSerial {dev->ltrCrateRef().devInfo().serial()};
             if (!m_crateSerialList.contains(crateSerial)) {
                 if (crateSerial == selCrate)
                     fnd_ind = m_crateSerialList.size();
                 m_crateSerialList.append(crateSerial);
                 m_crateTypes.append(dev->ltrCrateRef().devInfo().type().deviceModificationName());
             }
             dev->close();
         }
     }

     m_crateBox->clear();
     int pos = 0;
     for (const QString &serial : crateSerialList()) {
         const QString &type {m_crateTypes.at(pos)};
         m_crateBox->addItem(QString{"%1 (%2)"}.arg(type, serial), serial);
         pos++;
     }

     if (m_crateBox->count()) {
        m_crateBox->setCurrentIndex(fnd_ind);
     }

     updateCrate(prefSlot, true);

     connect(m_crateBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
             this, &LTRModuleSelectionUI::onCrateChanged);

     if (m_devList.empty() && m_no_module_warn_en) {
         QMessageBox::warning(m_parent, tr("Не найден модуль"),
                              tr("Не было найдено ни одного модуля!\nПроверьте подключение крейта с модулями и нажмите '%1'")
                              .arg(m_resetBtnName),
                              QMessageBox::StandardButton::Ok);
     }
}

void LTRModuleSelectionUI::closeCurModule() {
    if (m_dev) {
        saveSettings();
        if (m_dev->isOpened()) {
            m_dev->close();
        }
        disconnect(m_dev.data(), &LQMeas::Device::opProgress,
                   this, &LTRModuleSelectionUI::onModuleOpProgr);
        m_dev = QSharedPointer<LQMeas::LTRModule>{};
    }
}

void LTRModuleSelectionUI::saveSettings() {
    if (m_dev) {
        QSettings set;
        set.beginGroup(m_set_group);
        set.setValue(ui_key_crate_serial, m_dev->ltrCrateRef().devInfo().serial());
        set.setValue(ui_key_crate_slot, m_dev->slot());
    }
}

void LTRModuleSelectionUI::onCrateChanged() {
    updateCrate();
}

void LTRModuleSelectionUI::updateCrate(int prefSlot, bool try_reopen) {
    disconnect(m_slotBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
               this, &LTRModuleSelectionUI::onModuleChanged);
    m_slotBox->clear();

    if (m_crateBox->currentIndex() >=0) {
        int fnd_ind = 0;
        const QString &crate_ser {m_crateSerialList.at(m_crateBox->currentIndex())};
        for (const QSharedPointer<LQMeas::LTRModule> &dev : moduleList()) {
            if (dev->ltrCrateRef().devInfo().serial() == crate_ser) {
                if (dev->slot()==prefSlot)
                    fnd_ind = m_slotBox->count();
                m_slotBox->addItem(QString{"%1: %2"}
                                   .arg(dev->location(), dev->modificationName()),
                                   dev->slot());
            }
        }

        if (m_slotBox->count()) {
            m_slotBox->setCurrentIndex(fnd_ind);
        }
    }

    onModuleChanged(try_reopen);

    connect(m_slotBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                   this, &LTRModuleSelectionUI::onModuleChanged);
}

void LTRModuleSelectionUI::onModuleChanged(bool try_reopen) {
    int dev_ind {0};

    Q_EMIT beforeModuleChange();

    closeCurModule();

    if (m_slotBox->currentIndex() >= 0) {
        const QString crate_ser {m_crateSerialList.at(m_crateBox->currentIndex())};
        const int slot {m_slotBox->itemData(m_slotBox->currentIndex()).toInt()};
        for (int i=0; i < m_devList.size(); ++i) {
            if ((m_devList[i]->ltrCrateRef().devInfo().serial() == crate_ser)
                    && (m_devList[i]->slot() == slot)) {
                m_dev = m_devList[i];
                dev_ind = i;
                break;
            }
        }

        if (m_dev) {
            connect(m_dev.data(), &LQMeas::Device::opProgress,
                       this, &LTRModuleSelectionUI::onModuleOpProgr);

            LQError err;
            m_dev->open(m_open_flags, err);
            if (!m_dev->isOpened()) {
                m_dev = QSharedPointer<LQMeas::LTRModule>{};
                if (try_reopen && (++m_dev_reopen_cnt < m_devList.size())) {
                    if (++dev_ind == m_devList.size())
                        dev_ind = 0;
                    refreshDevList(m_devList[dev_ind]->ltrCrateRef().devInfo().serial(),
                                   m_devList[dev_ind]->slot());
                } else {
                    QMessageBox::critical(m_parent, tr("Ошибка открытия модуля"),
                                         tr("Не удалось установить связь с модулем: %1.\nВыбирите другой модуль или проверте подключение и нажмите '%2'")
                                          .arg(err.msg(), m_resetBtnName),
                                         QMessageBox::StandardButton::Ok);

                }
            }
        }
    }

    Q_EMIT moduleChanged(m_dev);
}

void LTRModuleSelectionUI::onModuleOpProgr(LQMeas::Device::OpStatus status, const QString &descr,
                                           int done, int size, const LQError &err) {
    Q_UNUSED(err)
    if (status == LQMeas::LTRModule::OpStatus::Start) {
        delete m_progrDialog;
        /* создаем диалог, который будем использовать для индикации прогресса модулей */
        m_progrDialog =  new QProgressDialog{QString(), 0, 0, 100, m_parent,
                Qt::WindowType::Dialog
                | Qt::WindowType::MSWindowsFixedSizeDialogHint
                | Qt::WindowType::CustomizeWindowHint
                | Qt::WindowType::WindowTitleHint};

        m_progrDialog->setMinimumWidth(350);
        m_progrDialog->setWindowModality(Qt::WindowModality::WindowModal);
        m_progrDialog->setMaximum(size);
        m_progrDialog->setValue(done);
        m_progrDialog->show();
        m_progrDialog->setWindowTitle(descr);
    } else if (status == LQMeas::LTRModule::OpStatus::Progress) {
        if (m_progrDialog)
            m_progrDialog->setValue(done);
    } else if (status == LQMeas::LTRModule::OpStatus::Finish) {
        if (m_progrDialog)
            m_progrDialog->hide();
    }
}
